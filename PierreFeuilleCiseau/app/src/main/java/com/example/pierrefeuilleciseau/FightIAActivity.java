package com.example.pierrefeuilleciseau;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FightIAActivity extends AppCompatActivity {

    private TextView TextViewJeu;
    private TextView TextViewScore;
    private byte idCoupJoueur;
    private byte scoreJoueur = 0;
    private byte scoreIA = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fight_ia);
        TextView TextViewJeu = findViewById(R.id.id_TextJeu);
        TextView TextViewScore = findViewById(R.id.id_TextScore);
    }


    private void mettreajourTextViewJeu(String message) {
        TextViewJeu.setText(message);
    }
    private void mettreajourTextViewScore (){
        TextViewScore.setText("Joueur " + scoreJoueur + " - " + scoreIA + " IA");
    }

    public byte jeuIa() {
        byte code = (byte) (Math.random() * (5 - 1));

        if (code == 1) {
            mettreajourTextViewJeu("l'IA joue une pierre");

        } else if (code == 2) {
            mettreajourTextViewJeu("l'IA joue une feuille");
        } else if (code == 3) {
            mettreajourTextViewJeu("l'IA joue des ciseaux");
        } else if (code == 4) {
            mettreajourTextViewJeu("l'IA joue un spock");
        } else if (code == 5) {
            mettreajourTextViewJeu("l'IA joue un lezard");
        }
        return code;
    }

    public void clickBtnPierre(View view) {
        idCoupJoueur = 1;
        if (scoreJoueur < 3 || scoreIA <3){
            jeuIa();
            if (jeuIa() == 1) {
                // draw
            } else if (jeuIa() == 2) {
                scoreIA++;
            } else if (jeuIa() == 3) {
                scoreJoueur++;
            } else if (jeuIa() == 4) {
                scoreIA++;
            } else if (jeuIa() == 5) {
                scoreJoueur++;
            }

            mettreajourTextViewScore();
        }

        else {
            mettreajourTextViewJeu("Partie Terminee");
        }

    }

    public void clickBtnFeuille(View view) {
        idCoupJoueur = 2;
        if (scoreJoueur < 3 || scoreIA <3){
            jeuIa();
            if (jeuIa() == 1) {
                scoreJoueur++;
            } else if (jeuIa() == 2) {
                //draw
            } else if (jeuIa() == 3) {
                scoreIA++;
            } else if (jeuIa() == 4) {
                scoreJoueur++;
            } else if (jeuIa() == 5) {
                scoreIA++;
            }

            mettreajourTextViewScore();
        }
        else {
            mettreajourTextViewJeu("Partie Terminee");
        }

    }

    public void clickBtnCiseaux(View view) {
        idCoupJoueur = 3;
        if (scoreJoueur < 3 || scoreIA <3){
            jeuIa();
            if (jeuIa() == 1) {
                scoreIA++;
            } else if (jeuIa() == 2) {
                scoreJoueur++;
            } else if (jeuIa() == 3) {
                //draw
            } else if (jeuIa() == 4) {
                scoreIA++;
            } else if (jeuIa() == 5) {
                scoreJoueur++;
            }

            mettreajourTextViewScore();
        }
        else {
            mettreajourTextViewJeu("Partie Terminee");
        }

    }


    public void clickBtnSpock(View view) {
        idCoupJoueur = 4;
        if (scoreJoueur < 3 || scoreIA <3){
            jeuIa();
            if (jeuIa() == 1) {
                scoreJoueur++;
            } else if (jeuIa() == 2) {
                scoreIA++;
            } else if (jeuIa() == 3) {
                scoreJoueur++;
            } else if (jeuIa() == 4) {
                //draw
            } else if (jeuIa() == 5) {
                scoreIA++;
            }

            mettreajourTextViewScore();
        }
        else {
            mettreajourTextViewJeu("Partie Terminee");
        }
    }

    public void clickBtnLezard(View view) {
        idCoupJoueur = 5;
        if (scoreJoueur < 3 || scoreIA <3){
            jeuIa();
            if (jeuIa() == 1) {
                scoreIA++;
            } else if (jeuIa() == 2) {
                scoreJoueur++;
            } else if (jeuIa() == 3) {
                scoreIA++;
            } else if (jeuIa() == 4) {
                scoreJoueur++;
            } else if (jeuIa() == 5) {
                //draw
            }

            mettreajourTextViewScore();
        }
        else {
            mettreajourTextViewJeu("Partie Terminee");
        }
    }

    public void ClickQuitter (View view){ finish();}

    public void clickReset (View view){
        scoreIA = 0;
        scoreJoueur = 0;
        mettreajourTextViewScore();
    }

/**
    public void fight() {
        byte scoreJoueur = 0;
        byte scoreIA = 0;

        while (scoreJoueur < 3 && scoreIA < 3) {
            jeuIa();
            if (idCoupJoueur == 1) {
                if (jeuIa() == 1) {

                } else if (jeuIa() == 2) {
                    scoreIA++;
                } else if (jeuIa() == 3) {
                    scoreJoueur++;
                } else if (jeuIa() == 4) {
                    scoreIA++;
                } else if (jeuIa() == 5) {
                    scoreJoueur++;
                }
            } else if (idCoupJoueur == 2) {
                if (jeuIa() == 1) {
                    scoreJoueur++;
                } else if (jeuIa() == 2) {

                } else if (jeuIa() == 3) {
                    scoreIA++;
                } else if (jeuIa() == 4) {
                    scoreJoueur++;
                } else if (jeuIa() == 5) {
                    scoreIA++;
                }
            } else if (idCoupJoueur == 3) {
                if (jeuIa() == 1) {
                    scoreIA++;
                } else if (jeuIa() == 2) {
                    scoreJoueur++;
                } else if (jeuIa() == 3) {

                } else if (jeuIa() == 4) {
                    scoreIA++;
                } else if (jeuIa() == 5) {
                    scoreJoueur++;
                }
            } else if (idCoupJoueur == 4) {
                if (jeuIa() == 1) {
                    scoreJoueur++;
                } else if (jeuIa() == 2) {
                    scoreIA++;
                } else if (jeuIa() == 3) {
                    scoreJoueur++;
                } else if (jeuIa() == 4) {

                } else if (jeuIa() == 5) {
                    scoreIA++;
                }
            } else if (idCoupJoueur == 5) {
                if (jeuIa() == 1) {
                    scoreIA++;
                } else if (jeuIa() == 2) {
                    scoreJoueur++;
                } else if (jeuIa() == 3) {
                    scoreIA++;
                } else if (jeuIa() == 4) {
                    scoreJoueur++;
                } else if (jeuIa() == 5) {

                }
            }

        }


    }
 **/
}
